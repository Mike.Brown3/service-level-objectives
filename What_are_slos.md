### How do we define a perception or, what is reliability?
Whether you like it or not, the reliability of your service is tied to its user's perception of its reliability.  You could have a wonderful set of dashboards relating to this service all showing green, but once someone calls to report a problem with it, and their boss calls, and there boss calls, all asking "What's the deal with our automated TikTok upload service?" It is as good as down.  You have to drop everything and figure out not only what the problem is, but also "why aren't I seeing it?" Generally, it's best to get used to thinking about this beforehand so that the amount of investigation done under stress is minimized.  Being better prepared for situations like these is a benefit that comes from thinking about a service in terms of what its users expect.

### At a High Level 
Before we really start in on this, it is important to get square on the terminology used throughout these docs.  Sure, there are new buzzwords that folks are throwing around that we want to nail down, but in early conversations we found that there were some common ones that we needed to come to terms with as well. The next section lays out the basic definitions we'll be using for these docs.

#### Basics
In early conversations about how we wanted to measure and communicate the reliability of our services a few terms kept coming up that although they weren't new, the way we were using them seemed, well, squishy.  So let's start by giving some specific definitions to these terms and hopefully avoid some confusion.

**User** - anything or anyone that relies on a service (internal/external folks, or another application or service)

**Service** - any system that has users.

**Uptime** - measurement of time an individual service has been actually running on a platform.

**Availability** - measurement of time an individual service is actually able to respond to requests by users.

**Reliability** - measurement of time an individual service is actually able to perform the duties it was designed to do.

#### SLI (Service Level Indicator)
- An SLI is the indicator for goodness.
- A good SLI measures your service from the perspective of your users.
- It should be simple and understandable by most stakeholders (not just IT folks) even if the underlying process that delivers the data is complex.
- Most useful if it can be a binary result.
- If the service is already established, user complaints will usually be a good place to start in terms of defining an SLI (or multiple)

#### SLO (Service Level Objective)
- The SLO is your objective for how often you can afford for it to fail or how often it needs to be "good".
- %age of "good" events by dividing it by the total number of events.
- "Proper level of reliability" targeted by the service.
- It is a single thing to point at in order to determine what constitutes an emergency.
- Nothing is perfect, and these are not designed to be long term.  As we learn more about the service chances are the SLOs will change or evolve.  So, setting SLOs is more of an iterative process than a once and done task.

#### Error budget
- Measures how an SLI has performed against the SLO over a period of time.
- Conversely, it measures how unreliable a service is permitted to be within the period and serves as a signal for when corrective action must be taken.
- Used to make decisions about how to prioritize work concerning the service. If it overshoots its error budget, spend more time addressing its reliability rather than focusing on new features.

#### SLAs (Service Level Agreements)
- Usually used in written contracts or companies big enough to charge back.
- And an SLA is an agreement with your users about it.
- This is the "Or else" to the SLO's "how often it needs to be good".

#### MTtD (Mean Time to Diagnosis)
- The time it takes for someone other than the developer owning the code to figure out what the underlying issue is with a service
- Usually, this would be a metric that would be used to show how good the project documentation and on-boarding training is and how changes made either improve or degrade it's quality.

#### MTtR (Mean Time to Resolution)
- The time it takes for a resolution to be achieved for a particular issue. 
- Used as a measurement of goodness for incident response.
- Requires discipline on the part of management and incident managers to be useful.

### What roles do we all play in making these work?
There are some things that we get mostly for free when designing for containerized workloads.  Kubernetes has the tooling for us to be able to tell if a service is up and available (basically, does making a request return a 200 status code).  What it can't do is allow us to automatically tell if a service is reliable at any deeper level than that.   That's where the development teams can really shine.  They will know better than anyone what the specific paths through the code are and how to check to see if the service is doing what it is designed to do.  We want to leverage that knowledge in determining a service's reliability.

### Why what we measure against is important. 
Imagine being on-call.  It's the middle of the night and you're enjoying your favorite dream.  Just as you're getting to your favorite part, you get woken up by a page.  You look at your phone and it says "Level: Severe.  Intermediate buffer threshold levels have been passed. Investigate immediately" and the only thing you can think of as you shake the dream out of your head is "What's an Intermediate buffer?"  
No one wants to be on the receiving end of one of those pages.  It's alerting on a very narrow set of data and doesn't really communicate what the impact is.  Usually, it can't because it just doesn't know.  That is why we start the process by looking at the service for a user's perspective and use that to drive how things are defined.  By doing that, there's fewer (if any) transient errors waking you out of your dreams at night just to be resolving themselves by the time your laptop boots.  
When you do end up getting paged, there are some other benefits to framing reliability from a user perspective that will improve everyone's standard of living.

1. The work you put into defining these alerts will have you asking the same questions the users will, so you'll not only see an issue you might have missed before, but you'll also see it before the users because it's check has been automated.
2. The work you put into defining these alerts will speed up the time it takes to diagnose an issue (MTtD)
3. If you use the information you gather as part of this work to improve the support and onboarding documentation it'll speed up the time it takes to reach a resolution (MTtR)
4. Communication, even to upper management is improved because, well, the alert tells you exactly what's wrong in plain English, like "Users are not receiving an interactive page when logging in", which doesn't need to be interpreted to be sent up the chain when reporting the issue.



