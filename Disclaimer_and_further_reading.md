### Disclaimer and Further Reading
I don't think there is anything in these documents that is strikingly original or worthy of claiming as "mine".  The few insights I do have at the time of this writing are due to having experienced the trouble of a poorly designed be it at the management, team, or on-call level.   The views I have (as well as you, I'll reckon) are based on those frustrating situations where either you have the information and no control over the situation or exactly the opposite and having to stew in it.  That said, here are some resources that have been helping me make sense of the situations and how to make things better.

Books:

Hidalgo, A. (2020) _Implementing Service Level Objectives_, O'Reilly

Titmus, M.A. (2021) _Cloud Native Go_, O'Reilly

Blogs:

Majors, Charity _[wharity.wtf](https://charity.wtf/)_ 
    Her Twitter account has more off the cuff content. @mipsyTipsy

_[nobl9.com/resources](https://nobl9.com/resources/)_ Alex Hidalgo's company that has their own SLO platform.  Their blog has some nifty points.

web sites:

_[openslo.com](https://openslo.com/)_ spec for declaritively defining reliability and performance targets.
