# Service Level Objectives (SLOs)

One of the many changes we're working on implementing across the company is measuring our services in terms of Service Level Objectives, or SLOs. The switch is being made to help us align closer with one of our core values, "Start with the customer", but as you will see, it will also help us to be more effective for determining when there is an issue and communicate better across all levels of business. These documents will discuss, at a basic level, what SLOs are, how they will help us move forward (and save our sanity), and how to start thinking about implementing them for your team's services.

### The Scope of these Documents
The infrastructure teams at Angi have a lot of projects in flight.  Although the concepts were talking about here could help in any of our environments and platforms, these documents are looking to specifically improve our users experiences in the Cloud environments we are currently deploying into.

### What are SLOs?
How do we define a perception or, what is reliability?

At a High Level

What roles do we all play in making these work

Why what we measure against is important. 


### Why are they important at Angi?
How can they help us generally

Legacy maintenance

Cloud Sanity

### How to Start?
Framing the perspective

Why change sometimes sounds difficult 

Questions to ask

What the infrastructure teams provide

### What is already in place?
Showing services up & available

AWS health generally

load balancers into clusters working

Other aaS provider health status

### Disclaimer and Further Reading