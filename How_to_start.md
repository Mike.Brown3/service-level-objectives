### How to Start?
Sometimes taking the first step is the hardest.  This document is going to lay out some inspiration and ideas to help folks get started.
#### Framing the perspective
If we are going to sum up this process, the best way to put it is that we want to be able to use our systems to answer the question, "*Is my service doing what its users need it to do?*"  The closer we come to being able to define what those expectations are and implement ways to validate those expectations, the more successful we will be.

#### Why change sometimes sounds difficult
New things can be exciting, but they often involve change and for most companies that change involves uncertainty.  It's where that uncertainty comes up that fear sets in.  The bigger the change, the more uncertainty, more fear.  The hard reality is that stagnancy has been keeping you from your full potential, here are a couple of things to help you get over on these difficulties.

**No matter how hard you try, nothing will ever be perfect.**  It can't possibly be expected for you to come up with a perfect definition because that requires you to completely understand the users of your system.  Chances are good that you don't understand them to that level or even know the full extent of who/what those users are, and that's okay.  Implementing to the level of our current understanding gets us further than we are today and progress is our goal.

**This is not a one shot deal.**  Setting up definitions for customer expectations will always be evolving because your customer's expectations will always be changing.  So, think of this as an iterative process.  The sooner you get started, the sooner you'll be able to start to refine your process.

**Start where you are.** Chances are that you know something about this service that no one else does and how that plays into what folks expect of it.  Just going through the process of elaborating on that will document those expectations and give a baseline to refer to going forward. Think of it in terms like this:

&emsp;&emsp;  Basic level: **Uptime** - Measurement of time an individual service is actually running on a platform. Its mere existence at each check time is enough to satisfy uptime.

&emsp;&emsp;  Better level: **Availability** - measurement of time an individual service is actually able to respond to requests by users. Healthchecks for pods should be reflective of this level.

&emsp;&emsp;  Best level: **Reliability** - measurement of time an individual service is actually able to perform the duties it was designed to do. This level has to be defined in terms of the service's SLI and that SLI has to be based in what the users need the service to do to be effective.



#### Questions to ask
"*Start at the beginning*" is usually where we tell folks to start.  But, that is vague and daunting for folks who've never done this before.  Although eventually, you should get to pouring over design docs in an attempt to do your due diligence, I'd propose a different path to start with, but yeah, "*What did the design docs ask for?*" is certainly a question to ask.

"*What complaints have the team received about the service?*" This is going to be the best indicator for what users of your system are expecting.  They have literally told you what they expect and that it's not doing it.  This is about as close to having users do your job as I can think of.  Not only do they tell you what they are looking at, they are telling you what the expectation is, and with a bit of poking I'll bet they'd be able to articulate what a reasonable rate of failure would be.  

"*Have there been feature requests made for this service to make it easier to work with?*" Along with specific complaints, feature requests can be excellent indicators that folks are writing code to accommodate the eccentricities of this service.  Reviewing that code will show you what the users are expecting from the service.

If this is a new service or the other questions haven't been much of a help you could always start with this list of questions:
- "*Is the service up?*"
- "*Is the service available?*"
- "*Is the service responsive?*"
- "*Is the ratio of good responses to errors within reason?*"
- "*Are the responses in the correct data format?*"
- "*Are the responses sane in light of the requests?*"

Again, if you're feeling like the service you are trying to measure is too complicated to do this process, take a step back and ask yourself what are the users of this service actually care about it doing?

#### What the infrastructure teams provide
Once you've come up with answers to the questions around what defines if a service is doing what it was designed to do, the platforms and tools we currently have in place can help you to implement a way to evaluate those questions.  Most of the time this will be evaluating the metrics that you've put in place, so using a health check or alerting manager to make that defined call. Other times, it might involve having an outside service evaluate the service to make a "goodness" determination.  

The infrastructure teams are here to provide a way for these types of solutions to be defined in code and implemented in the same way the service's code is delivered.

